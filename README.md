# Puppet on Windows POC

Objectives:

- Manage Windows patching settings and cope with auto-updates

- Version control installed .NET applications

- .NET config in terms of machine.config and web.config under the inetpub directories
